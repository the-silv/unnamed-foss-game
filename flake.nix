{
  inputs = {
    devshell.url = "github:numtide/devshell";
    nixpkgs.url = "github:nixos/nixpkgs/nixpkgs-unstable";
    tiddly-nix.url = "git+https://codeberg.org/nameless_capybaras/tiddly-nix";
    utils.url = "github:numtide/flake-utils";
  };

  outputs = inputs:
    inputs.utils.lib.eachSystem (with inputs.utils.lib.system; [
      aarch64-linux
      i686-linux
      x86_64-linux
    ]) (
      system: let
        pkgs = import inputs.nixpkgs {
          inherit system;
          overlays = [inputs.devshell.overlays.default];
        };

        tools = let
          godot = pkgs.godot_4;

          gdscript = {
            name,
            file,
            substitutions ? {},
          }:
            pkgs.writeShellApplication {
              inherit name;
              runtimeInputs = [
                godot
              ];
              text = "${godot}/bin/godot4 --no-header --headless -s ${pkgs.substituteAll {
                src = file;
                env = substitutions;
              }} \"$@\" 2>/dev/null";
            };

          gdtoolkit = with pkgs;
            gdtoolkit_4.overridePythonAttrs (old: {
              pname = "gdtoolkit";
              version = "master";

              src = fetchFromGitHub {
                owner = "Scony";
                repo = "godot-gdscript-toolkit";
                rev = "b4f9cb91f6a4c51197143e4a888f77ed02550fff";
                hash = "sha256-GS1bCDOKtdJkzgP3+CSWEUeHQ9lUcAHDT09QmPOOeVc=";
              };
            });
        in {
          inherit godot gdtoolkit;

          tiddlywiki = pkgs.nodePackages.tiddlywiki;

          check-project = gdscript {
            name = "check-project";
            file = ./script/check_project.gd;
            substitutions = {
              alejandra = "${pkgs.alejandra}/bin/alejandra";
              gdformat = "${gdtoolkit}/bin/gdformat";
              gdlint = "${gdtoolkit}/bin/gdlint";
            };
          };

          todo-parser = gdscript {
            name = "todo-parser";
            file = ./script/todo_parser.gd;
          };
        };
      in {
        checks = {
          check-project = tools.check-project;
        };

        devShells.default = pkgs.devshell.mkShell {
          imports = ["${inputs.devshell}/extra/git/hooks.nix"];

          devshell = {
            packages = pkgs.lib.attrValues tools;
            motd = ''
              {bold}{120} ━━━ Ultimate UDFV Devshell 8000 ━━━{reset}
              $(${tools.todo-parser}/bin/todo-parser -- --list)
              $(menu)
            '';
          };

          git.hooks = {
            enable = true;

            pre-commit = {
              text = ''
                #!${pkgs.bash}/bin/bash
                ${tools.check-project}/bin/check-project
              '';
            };
          };

          commands = [
            {
              name = "gd";
              help = "launches the Godot editor";
              command = ''
                godot4 --editor > /dev/null 2> /dev/null & disown
              '';
            }
            {
              name = "wiki";
              help = "serves an editable copy of the game's wiki at http://localhost:8080";
              command = ''
                  tiddlywiki \
                  +plugins/tiddlywiki/filesystem \
                  +plugins/tiddlywiki/tiddlyweb \
                +plugins/tiddlywiki/codemirror \
                +plugins/tiddlywiki/codemirror-closetag \
                +plugins/tiddlywiki/codemirror-closebrackets \
                  wiki/ --listen
              '';
            }
            {
              name = "check-project";
              help = "checks if the project can be committed";
              package = tools.check-project;
            }
            {
              name = "todos";
              help = "prints a fancy todo list";
              command = "todo-parser -- --list";
            }
          ];
        };

        formatter = pkgs.alejandra;

        packages = with inputs.tiddly-nix.lib; {
          wiki-html = tiddlyHtml {
            inherit pkgs;
            src = ./wiki;
            name = "wiki-html";
          };

          wiki-serve = tiddlyServe {
            inherit pkgs;
            src = ./wiki;
            name = "wiki-serve";
          };
        };
      }
    );
}
