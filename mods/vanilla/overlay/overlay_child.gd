extends CapyCore.GameEffect

const Parent = preload("res://mods/vanilla/overlay/overlay.gd")
const HOOK_GET_NODE: String = Parent.HOOK_GET

var scene: PackedScene
var instance: Node


func get_node() -> Node:
	if instance is Node:
		return instance

	if scene is not PackedScene:
		return null

	instance = scene.instantiate()
	return instance


func _get_hooks() -> Array[Hook]:
	return [Hook.from(HOOK_GET_NODE, get_node)]
