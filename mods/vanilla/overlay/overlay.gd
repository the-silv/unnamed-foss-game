extends CapyCore.GameEffect

const Child: GDScript = preload("res://mods/vanilla/overlay/overlay_child.gd")
const HOOK_GET: String = "overlay.get_node"
const HOOK_BIND: String = "overlay.bind_parent"

var overlays: Array[Node]
var parent_node: Node


func _parse(new_id: String, new_value: Variant, new_status: Status, new_src: String) -> GameEffect:
	if new_status != Status.TRUSTED:
		return null

	if new_value is not String:
		return null

	var value_str: String = new_value
	var loaded_res: Resource = load(new_src.path_join(value_str))

	if loaded_res is not PackedScene:
		return null

	var loaded_scene: PackedScene = loaded_res

	var new_effect: Child = Child.new()
	new_effect.setup(new_id, new_status, new_src)
	new_effect.scene = loaded_scene

	return new_effect


func _post_add(object: GameObject) -> void:
	report(
		object.updated.connect(update_overlays.bind(object)) as Error,
		"connecting %s to %s.updated" % [self, object]
	)


func _get_hooks() -> Array[Hook]:
	return [Hook.from(HOOK_BIND, bind_parent)]


func bind_parent(node: Node) -> void:
	if parent_node is Node:
		for overlay: Node in overlays:
			parent_node.remove_child(overlay)

	parent_node = node

	for overlay: Node in overlays:
		parent_node.add_child(overlay)


func update_overlays(object: GameObject) -> void:
	if parent_node is Node:
		for overlay: Node in overlays:
			parent_node.remove_child(overlay)

	overlays.clear()

	var results: Array = object.call_hook(HOOK_GET)

	for result: Variant in results:
		if result is not Node:
			continue

		var result_node: Node = result
		overlays.append(result_node)

		if parent_node is Node:
			parent_node.add_child(result_node)

	var print_fn: Callable = func() -> void:
		for overlay: Node in overlays:
			print(" - ", overlay)

		print()

		for effect: GameEffect in object.effects.get_all():
			if effect != self:
				print(" - ", effect)

	if not overlays.is_empty():
		CapyLib.print_section("Overlays", print_fn)
