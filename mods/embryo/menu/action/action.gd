@tool
extends Control

signal pressed(what: Control)

@export var text: String = "":
	set(value):
		text = value
		($Text as RichTextLabel).text = "[center][unskew][b]" + text
@export var enabled: bool = true:
	set(value):
		enabled = value
		($Button as Button).disabled = not enabled
		($Text as RichTextLabel).self_modulate = (
			Color(1.0, 1.0, 1.0, 1.0) if enabled else Color(1.0, 1.0, 1.0, 0.5)
		)


func _on_button_pressed() -> void:
	pressed.emit(self)


func child_grab_focus() -> void:
	($Button as Control).grab_focus()
