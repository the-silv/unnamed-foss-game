@tool
extends EditorPlugin
# TODO: 0.2.2: Actually make the Test plugin something useful

const TEST_DIR: String = "res://addons/test/test"


func _build() -> bool:
	return CapyLib.print_section("Starting Testing...", test_all, "Tests Done")


func test_all() -> bool:
	for test: String in DirAccess.get_files_at(TEST_DIR):
		if not test.ends_with(".gd"):
			continue

		var script: GDScript = load(TEST_DIR.path_join(test))

		var instance: Object = script.new()

		if not instance.has_method("_test"):
			continue

		@warning_ignore("unsafe_method_access")
		if not instance._test():
			push_error(" - %s: FAIL!" % test)
			return false

		print(" - %s: OK" % test)

	return true
