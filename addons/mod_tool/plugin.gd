@tool
extends EditorPlugin

const MAIN_SCREEN: PackedScene = preload(
	"res://addons/mod_tool/main_screen/content_editor/content_editor.tscn"
)
const DOCK_FOLDER: String = "res://addons/mod_tool/dock"

var main_screen_instance: Control
var docks: Dictionary = {
	# "mod_contents": {"slot": DOCK_SLOT_RIGHT_UL},
	"mod_list": {"slot": DOCK_SLOT_LEFT_UR},
}


func _enter_tree() -> void:
	main_screen_instance = MAIN_SCREEN.instantiate()
	EditorInterface.get_editor_main_screen().add_child(main_screen_instance)
	_make_visible(false)

	for dock_name: String in docks.keys():
		var dock_slot: DockSlot = docks[dock_name].slot
		var dock_scene: PackedScene = load(DOCK_FOLDER.path_join(dock_name).path_join("dock.tscn"))
		var dock_instance: Control = dock_scene.instantiate()
		docks[dock_name].dock_instance = dock_instance
		add_control_to_dock(dock_slot, dock_instance)


func _exit_tree() -> void:
	if main_screen_instance:
		main_screen_instance.queue_free()

	for dock_name: String in docks.keys():
		var instance: Control = docks[dock_name].dock_instance
		remove_control_from_docks(instance)
		instance.free()


func _has_main_screen() -> bool:
	return true


func _make_visible(visible: bool) -> void:
	if main_screen_instance is not Control:
		return

	main_screen_instance.visible = visible


func _get_plugin_name() -> String:
	return "Mod Editor"


func _get_plugin_icon() -> Texture2D:
	return EditorInterface.get_editor_theme().get_icon("Edit", "EditorIcons")
