@tool
extends EditorPlugin

var singletons: Array[Array] = [
	["Game", "game/game.gd"],
]


func _enable_plugin() -> void:
	for singleton: Array in singletons:
		var s_name: String = singleton[0]
		var s_path: String = singleton[1]
		add_autoload_singleton(s_name, plugin_path(s_path))


func _disable_plugin() -> void:
	for singleton: Array in singletons:
		var s_name: String = singleton[0]
		remove_autoload_singleton(s_name)


func plugin_path(suffix: String) -> String:
	var script: Script = get_script()
	return script.get_path().get_base_dir().path_join(suffix)
