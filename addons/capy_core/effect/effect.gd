@tool
extends CapyCore.Internals.Base

enum Status {
	DISABLED,
	UNTRUSTED,
	TRUSTED,
}

const Set = CapyCore.Internals.Set
const GameObject = CapyCore.GameObject
const GameEffect = CapyCore.GameEffect

var id: String:
	set(new):
		if id == new:
			return

		id = new
		batch_update()

var status: Status = Status.DISABLED:
	set(new):
		if new == status:
			return

		status = new
		batch_update()

var src: String:
	set(new):
		if new == src:
			return

		src = new
		batch_update()

var objects: Set = batch_make_child(
	Set.create(
		[func(value: Variant) -> bool: return value is GameObject, _pre_add],
		[_pre_remove],
		[_post_add],
		[_post_remove],
	),
	false,
	true
)


func setup(new_id: String, new_status: Status, new_src: String) -> void:
	var fn: Callable = func() -> void:
		id = new_id
		status = new_status
		src = new_src

	batch_run(fn)


static func hook(from_id: String, from_fn: Callable) -> Hook:
	return Hook.from(from_id, from_fn)


func _parse(_id: String, _value: Variant, _status: Status, _src: String) -> GameEffect:
	return null


func _get_hooks() -> Array[Hook]:
	return []


func _pre_add(_object: GameObject) -> Error:
	return OK


func _pre_remove(_object: GameObject) -> Error:
	return OK


func _post_add(_object: GameObject) -> void:
	pass


func _post_remove(_object: GameObject) -> void:
	pass


func _to_string() -> String:
	return "Effect(%s)" % id


class Hook:
	extends RefCounted

	var id: String
	var function: Callable

	static func from(new_id: String, new_fn: Callable) -> Hook:
		var new: Hook = Hook.new()
		new.id = new_id
		new.function = new_fn
		return new
