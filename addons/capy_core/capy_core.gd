class_name CapyCore extends RefCounted

const PATH: String = "res://addons/capy_core/"  # remeber the trailing /

const GameEffect = preload(PATH + "effect/effect.gd")
const GameObject = preload(PATH + "object/object.gd")
const Game = preload(PATH + "game/game.gd")


class Internals:
	extends RefCounted
	const SUB_PATH: String = PATH + "internals/"
	const Set = preload(SUB_PATH + "set/set.gd")
	const Base = preload(SUB_PATH + "base/base.gd")
	const Report = preload(SUB_PATH + "report/report.gd")


class Effects:
	extends RefCounted
	const DummyEffect = preload(PATH + "common/effect/dummy_effect.gd")
	const ScriptEffect = preload(PATH + "common/effect/script_effect.gd")
	const ValueEffect = preload(PATH + "common/effect/value_effect.gd")
