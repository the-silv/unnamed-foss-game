@tool
extends CapyCore.Internals.Base

const Set = CapyCore.Internals.Set
const GameEffect = CapyCore.GameEffect
const GameObject = CapyCore.GameObject

var effects: Set = batch_make_child(
	Set.create(
		[
			func(val: Variant) -> bool: return val is GameEffect,
			_effect_pre_add,
		],
		[_effect_pre_remove],
		[_effect_post_add],
		[_effect_post_remove],
	),
	true,
	true
)
var description: String
var _filter: Callable:  # Callable(GameEffect) -> bool
	set(value):
		_filter = value
		batch_update()
var _hooks: Dictionary  # { id : Hook }


static func create_custom(custom_filter: Callable, custom_description: String) -> GameObject:
	var new: GameObject = GameObject.new()

	new._filter = custom_filter
	new.description = custom_description

	return new


static func create_standard(
	id_query: String, min_status: GameEffect.Status, custom_description: String = ""
) -> GameObject:
	var new: GameObject = GameObject.new()

	new._filter = func(fx: GameEffect) -> bool:
		if not fx.id.begins_with(id_query):
			return false

		if fx.status < min_status:
			return false

		return true

	new.description = custom_description
	if custom_description.is_empty():
		new.description = (
			"picks any effect that begins with %s and has a min status of %s"
			% [id_query, GameEffect.Status.find_key(min_status)]
		)

	return new


func union(other: GameObject) -> GameObject:
	var new: GameObject = GameObject.new()

	new._filter = func(fx: GameEffect) -> bool: return _filter.call(fx) or other._filter.call(fx)
	new.description = "union(%s, %s)" % [description, other.description]

	return new


func intersection(other: GameObject) -> GameObject:
	var new: GameObject = GameObject.new()

	new._filter = func(fx: GameEffect) -> bool: return _filter.call(fx) and other._filter.call(fx)
	new.description = "intersection(%s, %s)" % [description, other.description]

	return new


func call_hook(id: String, args: Array = []) -> Array:
	if not _hooks.has(id):
		return []

	var hook: Hook = _hooks[id]

	return hook.call_functions(args)


func _to_string() -> String:
	return (
		'Object("%s", %s)'
		% [
			description,
			CapyLib.join(
				Array(
					effects.get_all().map(func(fx: GameEffect) -> String: return fx.id),
					TYPE_STRING,
					"",
					null
				),
				", "
			)
		]
	)


func _effect_pre_add(effect: GameEffect) -> Error:
	if not _filter.call(effect):
		return ERR_INVALID_PARAMETER

	var err: Error = effect.objects.add(self)

	return err


func _effect_post_add(effect: GameEffect) -> void:
	for effect_hook: GameEffect.Hook in effect._get_hooks():
		var new_hook: Hook = _hooks.get_or_add(effect_hook.id, Hook.create(effect_hook.id))

		report(
			new_hook.add(effect, effect_hook),
			"connecting effect %s to hook %s" % [effect.id, new_hook.id]
		)


func _effect_pre_remove(effect: GameEffect) -> Error:
	return effect.objects.remove(self)


func _effect_post_remove(effect: GameEffect) -> void:
	for hook: Hook in _hooks.values():
		report(hook.remove(effect), "cleaning up %s" % effect, [], Report.Severity.INFO)


class Hook:
	extends RefCounted

	var id: String
	var functions: Dictionary  # { effect : Callable }

	static func create(new_id: String) -> Hook:
		var new: Hook = Hook.new()
		new.id = new_id
		return new

	func add(effect: GameEffect, hook: GameEffect.Hook) -> Error:
		if functions.has(effect):
			return ERR_ALREADY_EXISTS

		if hook.id != id:
			return ERR_INVALID_PARAMETER

		functions[effect] = hook.function
		return OK

	func remove(effect: GameEffect) -> Error:
		if not functions.has(effect):
			return ERR_DOES_NOT_EXIST

		return OK if functions.erase(effect) else FAILED

	func call_functions(args: Array) -> Array:
		var collector: Array = []
		for function: Callable in functions.values():
			collector.append(function.callv(args))
		return collector
