extends CapyCore.GameEffect


func _init() -> void:
	setup("core.gdscript", Status.TRUSTED, "")


func _parse(new_id: String, new_value: Variant, new_status: Status, new_src: String) -> GameEffect:
	if new_status != GameEffect.Status.TRUSTED:
		return null

	if new_value is not String:
		return null

	var value_str: String = new_value

	var script: GDScript = load(new_src.path_join(value_str))

	if script.get_base_script() != GameEffect:
		return null

	var new_instance: GameEffect = script.new()
	new_instance.setup(new_id, new_status, new_src)

	return new_instance
