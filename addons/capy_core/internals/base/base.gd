@tool
extends RefCounted

signal updated
signal update_checked
signal report_emitted(report: Report)

const Base = CapyCore.Internals.Base
const Report = CapyCore.Internals.Report

var _batch_changed: bool = false
var _batch_parent_ref: WeakRef
var _batch_parent: Base = null:
	set(value):
		_batch_parent_ref = weakref(value)
	get():
		if _batch_parent_ref is not WeakRef:
			return null
		return _batch_parent_ref.get_ref()

var _batch_levels: int = 0:
	set(value):
		_batch_levels = max(0, value)

var _reports_silence_levels: int = 0:
	set(value):
		_reports_silence_levels = max(0, value)


func _to_string() -> String:
	return "Base(%s)" % get_instance_id()


func batch_set_parent(parent: Base) -> Error:
	if _batch_parent is Base:
		_batch_parent.update_checked.disconnect(batch_update.bind(false))
		_batch_parent = null

	if parent is not Base:
		return OK

	var err: Error = parent.update_checked.connect(batch_update.bind(false)) as Error
	if err:
		return err

	_batch_parent = parent

	return OK


func batch_make_child(child: Base, connect_update: bool = false, set_parent: bool = false) -> Base:
	if connect_update:
		report(
			child.updated.connect(batch_update),
			"connecting updated() while adding %s as batch child" % child
		)

	if set_parent:
		report(child.batch_set_parent(self), "setting parent while adding batch child %s" % child)

	return child


func batch_update(changed: bool = true) -> void:
	if changed:
		_batch_changed = true

	if _batch_changed and _batch_get_lvl() <= 0:
		_batch_changed = false
		updated.emit()

	update_checked.emit()


func batch_run(fn: Callable) -> void:
	_batch_start()
	fn.call()
	_batch_end()


func _batch_get_lvl() -> int:
	if _batch_parent is Base:
		return _batch_levels + _batch_parent._batch_get_lvl()

	return _batch_levels


func _batch_start() -> void:
	_batch_levels += 1


func _batch_end() -> void:
	_batch_levels -= 1
	batch_update(false)


func report(
	value: Variant,
	action: String,
	expected: Array[Variant] = [],
	severity: Report.Severity = Report.Severity.WARN,
) -> void:
	var new_report: Report = Report.new()

	new_report.emitter = self
	new_report.action = action
	new_report.value = value
	new_report.severity = severity

	if expected.has(value):
		return

	if [true, OK, 0].has(value) and expected.is_empty():
		return

	report_emitted.emit(new_report)

	if _reports_silence_levels > 0:
		return

	match new_report.severity:
		Report.Severity.WARN:
			push_warning(new_report)
		Report.Severity.CRIT:
			push_error(new_report)


func catch_reports(fn: Callable, silence_reports: bool = false) -> Array[Report]:
	var temp_reports: Array[Report] = []
	var append_fn: Callable = func(rep: Report) -> void: temp_reports.append(rep)

	report(
		report_emitted.connect(append_fn),
		"connecting report_emitted in Base.catch_reports()",
		[],
		Report.Severity.CRIT
	)
	_reports_silence_levels += 1 if silence_reports else 0

	fn.call()

	_reports_silence_levels -= 1 if silence_reports else 0
	report_emitted.disconnect(append_fn)

	return temp_reports
