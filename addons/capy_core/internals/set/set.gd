extends CapyCore.Internals.Base

signal added(value: Variant)
signal removed(value: Variant)

const Set = CapyCore.Internals.Set

var _values: Dictionary = {}  # { value : value }
var _pre_add: Array[Callable] = []
var _pre_remove: Array[Callable] = []


func _to_string() -> String:
	return "Set{ %s }" % CapyLib.join(Array(get_all().map(str), TYPE_STRING, "", null), ", ")


func _check_filter(filter: Callable) -> Error:
	var filter_result: Variant = filter.call()

	if filter_result is bool:
		return OK if filter_result else ERR_INVALID_PARAMETER

	if filter_result is Error:
		return filter_result

	return FAILED


static func create(
	pre_add: Array[Callable] = [],
	pre_remove: Array[Callable] = [],
	post_add: Array[Callable] = [],
	post_remove: Array[Callable] = [],
) -> Set:
	var new: Set = Set.new()

	new._pre_add = pre_add
	new._pre_remove = pre_remove

	for fn: Callable in post_add:
		new.report(new.added.connect(fn), "connecting post_add function in Set")

	for fn: Callable in post_remove:
		new.report(new.removed.connect(fn), "connecting post_remove function in Set")

	return new


func add(value: Variant) -> Error:
	if _values.has(value):
		return ERR_ALREADY_EXISTS

	for filter: Callable in _pre_add:
		var filter_err: Error = _check_filter(filter.bind(value))
		if filter_err != OK:
			return filter_err

	_values[value] = value
	added.emit(value)
	batch_update()
	return OK


func add_set(other: Set) -> Error:
	var fn: Callable = func() -> void:
		for value: Variant in other.get_all():
			report(add(value), "adding %s in add_set()" % value, [OK, ERR_INVALID_PARAMETER])

	batch_run(other.batch_run.bind(fn))
	return OK


func remove(value: Variant) -> Error:
	if not _values.has(value):
		return ERR_DOES_NOT_EXIST

	for filter: Callable in _pre_remove:
		var filter_err: Error = _check_filter(filter.bind(value))
		if filter_err != OK:
			return filter_err

	@warning_ignore("return_value_discarded")  # The existance of `value` is already assured
	_values.erase(value)

	removed.emit(value)
	batch_update()

	return OK


func remove_set(other: Set) -> Error:
	var fn: Callable = func() -> void:
		for value: Variant in other.get_all():
			report(remove(value), "removing %s in add_set()" % value, [], Report.Severity.INFO)

	batch_run(fn)
	return OK


func reload(value: Variant) -> Error:
	var err: Array[Error] = [OK]

	var fn: Callable = func() -> void:
		err[0] = remove(value)
		if err[0]:
			return

		err[0] = add(value)

	batch_run(fn)

	return err[0]


func has(value: Variant) -> bool:
	return _values.has(value)


func get_pre_add() -> Array[Callable]:
	return _pre_add.duplicate()


func get_pre_remove() -> Array[Callable]:
	return _pre_remove.duplicate()


func get_all() -> Array:
	return _values.keys()


func remove_all() -> Error:
	var err: Array[Error] = [OK]
	var fn: Callable = func() -> void:
		for value: Variant in get_all():
			err[0] = remove(value)
			if err[0] != OK:
				return

	batch_run(fn)

	return err[0]
