extends RefCounted

enum Severity {
	INFO,
	WARN,
	CRIT,
}

const Base = CapyCore.Internals.Base

var emitter: Base
var action: String
var severity: Severity
var value: Variant


func _to_string() -> String:
	var report: String = "%s: " % [Severity.find_key(severity)]

	if emitter is Base:
		report += "%s: " % emitter

	if not action.is_empty():
		report += "while %s: " % action

	if value is Error:
		var value_err: Error = value
		report += error_string(value_err)
	else:
		report += str(value)

	return report
