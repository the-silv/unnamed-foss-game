extends SceneTree

var proj_dir: String = ProjectSettings.globalize_path("res://")
var commands: Array[Command] = [
	Command.from("@gdlint@ %s" % proj_dir, "GDScript -- linter"),
	Command.from("@gdformat@ %s --check" % proj_dir, "GDScript -- formatter"),
	Command.from("@alejandra@ %s --check" % proj_dir, "Nix -- formatter"),
]


func _init() -> void:
	var err: Array[bool] = [false]

	var fn: Callable = func() -> void:
		for command: Command in commands:
			var exit_code: int = command.check()
			if exit_code != 0:
				err[0] = true

		commands.clear()

	CapyLib.print_section("Testing Project...", fn)

	quit(int(err[0]))


class Command:
	extends Resource

	var path: String
	var args: PackedStringArray
	var name: String

	static func from(command: String, from_name: String = "") -> Command:
		var new_command: Command = Command.new()

		var splitted_command: PackedStringArray = command.split(" ", false)

		var new_path: String = splitted_command[0]
		var new_args: PackedStringArray = splitted_command.slice(1)

		new_command.path = new_path
		new_command.args = new_args
		new_command.name = from_name

		if from_name.is_empty():
			new_command.name = new_command.path.get_file()

		return new_command

	func check() -> int:
		var output: Array = []
		var exit_code: int = OS.execute(path, args, output, true, false)

		if exit_code == 0:
			print_rich("[b]%s: [color=green]OK[/color][/b]" % name)

		else:
			print_rich("[b]%s: [color=red]ERR[/color][/b]" % name)
			var stdout: String = output[0]
			print("│ %s\n" % stdout.replace("\n", "\n│ "))

		return exit_code
