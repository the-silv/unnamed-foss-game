extends SceneTree

var todos: Array[Todo]


func _init() -> void:
	var args: PackedStringArray = OS.get_cmdline_user_args()

	var scanner: Callable = func(rec_self: Callable, path: String) -> void:
		for filepath: String in DirAccess.get_files_at(path):
			if not filepath.ends_with(".gd"):
				continue

			var file: FileAccess = FileAccess.open(path.path_join(filepath), FileAccess.READ)

			if file is not FileAccess:
				continue

			for line: String in file.get_as_text().split("\n", false):
				var todo: Todo = Todo.parse_line(line)
				if todo is not Todo:
					continue

				todos.append(todo)

		for subdir: String in DirAccess.get_directories_at(path):
			rec_self.call(rec_self, path.path_join(subdir))

	scanner.call(scanner, "res://")

	if args.has("--list"):
		if todos.is_empty():
			print_rich("[i]No Todos to show...[/i]")

		var sort_custom: Callable = func(a: Todo, b: Todo) -> bool:
			if a.severity > b.severity:
				return true

			if a.text.naturalnocasecmp_to(b.text) < 0 and a.severity == b.severity:
				return true

			return false

		todos.sort_custom(sort_custom)

		for todo: Todo in todos:
			var color: String
			match todo.severity:
				Todo.Severity.CRITICAL:
					color = "red"
				Todo.Severity.WARNING:
					color = "orange"
				Todo.Severity.NOTICE:
					color = "cyan"

			print_rich("[color=%s]  - [b]%s[/b]: [/color]%s" % [color, todo.type, todo.text])

	if args.has("--json"):
		pass  # TODO: 0.2.2: Implement --json todos

	quit(0)


class Todo:
	extends Resource

	enum Severity {
		NOTICE,
		WARNING,
		CRITICAL,
	}
	const SEPARATOR: String = ": "

	var type: String
	var severity: Severity
	var text: String

	static func parse_line(line: String) -> Todo:
		if not line.contains("# "):
			return null

		var comment: String = line.split("# ", true, 1)[1]

		if not comment.contains(SEPARATOR):  # check also excludes empty/invalid comments
			return null

		var new_type: String = comment.split(SEPARATOR, false)[0]
		var new_severity: Severity

		match new_type:
			"ALERT", "ATTENTION", "CAUTION", "CRITICAL", "DANGER", "SECURITY":
				new_severity = Severity.CRITICAL

			"BUG", "DEPRECATED", "FIXME", "HACK", "TASK", "TBD", "TODO", "WARNING":
				new_severity = Severity.WARNING

			"INFO", "NOTE", "NOTICE", "TEST", "TESTING":
				new_severity = Severity.NOTICE

			_:
				return null

		var new_todo: Todo = Todo.new()
		new_todo.type = new_type
		new_todo.severity = new_severity
		new_todo.text = comment.split(SEPARATOR, false, 1)[1]

		return new_todo
