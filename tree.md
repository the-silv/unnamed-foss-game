# Legend
- constructor: implements `_from_json()`
- manager: meant to run code on `_add()`


# Schema
- core
  - value
  - gdscript
-

- overlay (manager, constructor)
  - map : overlay = { scene = "..." }
  - camera : overlay = { scene = "..." }
  - menu : overlay = { scene = "..." }
-

- menu (manager)
  - slide (constructor)
    - ...
  - action/button (constructor)
    - ...
  - section (constructor)
    - ...
  - ...
-

- character (manager)
  - skin (manager?, constructor)
    - ...
  - ...
-

- map (manager)
  - ...
-
