# Ultimate Friendship Destroyer 5000 (UFD-V)
> **!!NOTE!! This is still a prototype, as now most of the stuff listed below are just big dreams**
> *We'll get there one day™*

## What is this?
This is a little 2D multiplayer arena with big ambitions, even though as the time of writing it's still in its early days.
Its main goals/features are:
- **Customize everything**
  Basically the game itself is considered as a mod, giving players the ability to change everything they want.
  This means:
    - **Custom game rules**
    - **Custom maps**
    - **Custom characters**
    - **Custom weapons**

- **Party mode and quick matches**
  Do you have the feeling that local-multiplayer games are no longer a thing?
  With party mode you'll be able to quickly set up matches to challenge your friends, and even if you're a lone wolf it won't take too long to get playing.

- **Friendly competition**
  Fight along or against your friends without worrying about any long-term points or score.
  The game also adapts to each one's skill level to keep itself challenging for everyone.

- **Free and Open Source**
  No microtransactions, no ads, no bullshit. Just a donate button if you're feeling fancy.

## [The Wiki](https://codeberg.org/nameless_capybaras/UFD-V/wiki)
It (probably) contains everything you need to know about this project and has the most recent information available,
there's also two local copies, both in the main branch (the wiki source), and an HTML version in the pages branch

## Downloading the game
You can get a prototype from [the release page of Codeberg](https://codeberg.org/nameless_capybaras/UFD-V/releases),
for now only Windows and Linux platforms are included.

## Project Setup
### With [Nix/NixOS](https://nixos.org/learn)

> **NOTE:** flakes need to be enabled

1. Download the project:
   ```bash
   git clone https://codeberg.org/nameless_capybaras/UFD-V.git
   ````

2. Use Nix to download all the required dependencies:
   ```bash
   nix develop
   ```
### Manual (not recommended)

1. Download the following dependencies:
   - [Git](https://git-scm.com/downloads)
   - [Godot](https://godotengine.org/download)
   - [Tiddlywiki](https://tiddlywiki.com/#Installing%20TiddlyWiki%20on%20Node.js)

2. Download the repository
   ```bash
   git clone https://codeberg.org/nameless_capybaras/UFD-V.git
   ```

- To edit the game just open the project in [[Godot]]

- To edit a local copy of this wiki (served on http://localhost:8080):
  ```bash
  tiddlywiki +plugins/tiddlywiki/filesystem +plugins/tiddlywiki/tiddlyweb wiki/ --listen
  ```

- To build an HTML version of this wiki (located at `wiki/output/index.html`):
  ```bash
  tiddlywiki wiki/ --render $:/core/save/all index.html text/plain
  ```

## I want to help!
Please take a look at the [developer wiki](https://codeberg.org/nameless_capybaras/UFD-V/wiki).
Feel free to post issues or pull requests, but we cannot guarantee you that we'll be able to do something about that.
